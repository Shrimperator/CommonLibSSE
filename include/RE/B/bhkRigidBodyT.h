#pragma once

#include "RE/B/BSTArray.h"
#include "RE/H/hkVector4.h"
#include "RE/H/hkQuaternion.h"
#include "RE/B/bhkRigidBody.h"

namespace RE
{
	class bhkRigidBodyT : public bhkRigidBody
	{
	public:
		inline static constexpr auto RTTI = RTTI_bhkRigidBodyT;
		inline static constexpr auto Ni_RTTI = NiRTTI_bhkRigidBodyT;

		~bhkRigidBodyT() override;  // 00

		hkQuaternion rotation; // 40
		hkVector4 translation; // 50
	};
	static_assert(offsetof(bhkRigidBodyT, rotation) == 0x40);
	static_assert(offsetof(bhkRigidBodyT, translation) == 0x50);
	static_assert(sizeof(bhkRigidBodyT) == 0x60);
}
